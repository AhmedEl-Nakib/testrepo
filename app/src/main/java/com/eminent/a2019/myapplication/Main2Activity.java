package com.eminent.a2019.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toast.makeText(this, "welcome to the other activity", Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "edits after merge", Toast.LENGTH_SHORT).show();
        //after merge push
        //last push in master
    }
}
